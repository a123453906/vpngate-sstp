# vpngate-sstp
自動抓取VPN gate VPN並自動連線

# Feature
* 自動抓取非219開頭IP之日本VPN (僅抓取具有SSTP協議)
* 自動建立VPN連線並隨機選取可用VPN連線

# Usage
* 有安裝python
  * python main.py [--name VPN name] "VPN name"填入欲建立VPN之名稱
* 或是直接從[release](https://gitlab.com/a123453906/vpngate-sstp/-/releases)下載 VPNGateSSTP.exe [--name VPN name] "VPN name"填入欲建立VPN之名稱
* name 選項可填可不填，不填的話預設會建立以"VPN-Japan"為名稱之VPN


