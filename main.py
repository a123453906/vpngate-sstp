import requests
from bs4 import BeautifulSoup
import random
import subprocess
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--name",
                    help="enter vpn name",
                    default="VPN-Japan")
args = parser.parse_args()

def run(self, cmd):
    completed = subprocess.run(["powershell", "-Command", cmd], capture_output=True)
    return completed

response = requests.get('https://www.vpngate.net/en/')
soup = BeautifulSoup(response.text,'html.parser')

canUse = False
list = []

for countryJP in soup.find_all('img',src='../images/flags/JP.png'):
    canUse = False
    for element in countryJP.next_elements:
        if "'\\n'" in repr(element):
            break
        # ip filter
        if "td" in repr(element) and 'opengw.net</span></b><br/><span style=' in repr(element)\
            and '>219.' not in repr(element)\
            and '>210.165.114.235' not in repr(element)\
            and '>111.96.70.21' not in repr(element):
            canUse = True
            ip = repr(element.find_all('span')[1].text)
        # sessions count
        if "sessions" in repr(element) and '</span><br/>Total' in repr(element) and canUse:
            if int(element.find('span').text.replace(' sessions','')) >= 3 and int(element.find('span').text.replace(' sessions','')) <= 80:
                pass
            else:
                canUse = False
        # speed
        if "Mbps" in repr(element) and 'Logging policy' in repr(element) and canUse:
            if float(element.find('span').text.replace(' Mbps','').replace(',','')) > 50.00:
                pass
            else:
                canUse = False
        # vpn has SSTP method
        if "SSTP Hostname" in repr(element) and 'SSTP Hostname :<br><b><span style=' in repr(element) and canUse:
            if 'SSTP Hostname' not in element.find('span').text:
                list.append(element.find('span').text)

randInt = random.randint(0, len(list)-1)
print(list[randInt])

vpn_name = args.name

getvpn_command = f"Get-VpnConnection -Name \"{vpn_name}\""

getvpn_info = run(0, getvpn_command)
if getvpn_info.returncode != 0:
    addvpn_cmd = f"Add-VpnConnection -Name \"{vpn_name}\" -ServerAddress {list[randInt]} -TunnelType \"Sstp\" -EncryptionLevel \"Optional\" -AuthenticationMethod MSChapv2 -RememberCredential -PassThru"
    run(0,addvpn_cmd)
    print(f"VPN is not exist, creating a new vpn with name '{vpn_name}'")
else:
    print("vpn is exist!")

setvpn_command_1 = f"Set-VpnConnection -Name \"{vpn_name}\" -ServerAddress {list[randInt]} -PassThru"
setvpn_info_1 = run(0,setvpn_command_1)
if setvpn_info_1.returncode != 0:
    print("An error occured: %s", setvpn_info_1.stderr)
else:
    print("set vpn address successfully!")
    print("connect to vpn, wait at least one minute")
    subprocess.Popen(f"rasdial \"{vpn_name}\" \"vpn\" \"vpn\"", stdout=subprocess.PIPE)